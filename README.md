# BioMecaNet Website Source

This repository contains all the files necessary to generate a static website using [Hugo](https://gohugo.io), 
and to publish it using [GitLab](https://gitlab.com)'s Pages 
(see [instructions](https://docs.gitlab.com/ee/user/project/pages/introduction.html)).


The website should be visible on at [http://biomeca.net](http://biomeca.net)  

And at the mirror site [https://biomecanet.gitlab.io/www/](https://biomecanet.gitlab.io/www/)

# Editing

The website is derived from the 'source files' contained in:

    - [content](content)
    - [static](static)

The `content` folder reflects the structure of the website, see [Hugo](https://gohugo.io/templates/lists/), and these files
can be encoded in [MarkDown](https://fr.wikipedia.org/wiki/Markdown) instead of HTML. 

Images and data files added to the `static` folder can be linked using
the '/' path in the Markdown sources. Images should go in `static/img` and data in `static/data`.

The web site is automatically re-generated if any file is updated (check the [Pipelines](pipelines)). This is done by Gitlab, and the process can take a minute or so.

# Gitlab

From Gitlab's website, it is possible to directly edit the files,
or to add content.

# Local work

All the work can be done on a local machine, using `GIT` and `Hugo`:

1. Clone the repository:

    git clone git@gitlab.com:biomecanet/www.git

2. Edit the files

	- any editor can be used, depedent files should be added
    
3. Check the result locally:

	hugo -D server --disableFastRender
   
This will run a local web server. Check the edits by opening [http://localhost:1313/www/](http://localhost:1313/www/).

# Adding content

Create a new file:

	hugo new content/NEW_FILE_NAME
	
[edit the file]  
[change 'draft: false' to 'draft: true']
		
	git add content/NEW_FILE_NAME

Commit changes:
	
	git commit content
	git push
	
# Adding images

Copy the image file to:

	static/img/NEW_IMAGE.png

You can then link the image in any page (.md file):

	![DESCRIPTION_OF_IMAGE](/img/NEW_IMAGE.png)  

Commit changes:
	
	git add static/img/NEW_IMAGE.png
	git commit static/img
	git push

## Example: a new post

Adding a new text file `example.md`:

    hugo new content/post/my_post.md

[edit the file]  
[change 'draft: false' to 'draft: true']

    git add content/post/my_post.md
    git commit content
    git push


## Theme

The BioMecaNet website uses [gohugo-theme](https://gitlab.com/biomecanet/gohugo-theme) which is a fork of [Ananke](https://github.com/theNewDynamic/gohugo-theme-ananke) with minor modifications.
