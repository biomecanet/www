---
title: "Bugtiply Day One"
date: 2022-06-14T14:23:35+01:00
draft: false
featured_image: "/img/bugtiply-player1.jpg"
---

![bugtiply](/img/bugtiply-player1.jpg)

After much development efforts by Carlos Lugo and Eashan Saikia, our game *Bugtiply* based on Cytosim met its first 40 players during the Festival of Plants in the Cambridge University Botanic Garden, on June 11 2022.

Thank you to all members of **BioMecaNet** for the feedback we received during the retreat!

# Bugtiply

{{<rawhtml>}} 
<video width=100% controls autoplay>
    <source src="/mov/bugtiply-day1.mp4" type="video/mp4">
    Your browser does not support the video tag.  
</video>
{{</rawhtml>}}

*An Evolution Game*

Bugtiply is an interactive game to explore the mechanism of mitosis – a type of cell division producing two identical daughter cells. Each simulated cell contains two chromosome copies (colored balls) pushed apart by filaments (white), with the help of molecular motors (small red lines). If the two chromosomes are pushed in opposite halves of the cell, the division of the cell in its middle will lead to correct partitioning: each daughter cell will inherit one copy of the chromosome. Incorrect partitioning occurs when one cell inherits two copies, and the other one gets none. In nature, this last configuration would likely lead to the death of both cells.

