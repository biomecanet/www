---
title: "Publications"
date: 2022-06-02T07:46:14+02:00
draft: false
---


# 2023

Effects of microtubule length and crowding on active microtubule network organization  
Wei-Xiang Chew, Gil Henkin, François Nédélec, Thomas Surrey  
https://doi.org/10.1016/j.isci.2023.106063

Stable kinetochore-microtubule attachment requires loop-dependent Ndc80-Ndc80 binding  
Soumitra Polley, Helen Müschenborn, Melina Terbeck, Anna De Antoni, Ingrid R Vetter, Marileen Dogterom, Andrea Musacchio, Vladimir A Volkov, Pim J Huis in 't Veld  
https://doi.org/10.15252/embj.2022112504

Microtubule binding of the human augmin complex is directly controlled by importins and Ran-GTP  
Kseniya Ustinova; Felix Ruhnow; Maria Gili; Thomas Surrey  
https://doi.org/10.1242/jcs.261096 


RZZ-Spindly and CENP-E form an integrated platform to recruit dynein to the kinetochore corona  
Verena Cmentowski, Giuseppe Ciossani, Ennio d'Amico, Sabine Wohlgemuth Mikito Owa, Brian Dynlacht, Andrea Musacchio  
https://doi.org/10.15252/embj.2023114838


# 2022

On the role of phase separation in the biogenesis of membraneless compartments   
A Musacchio  
https://doi.org/10.15252/embj.2021109952

Structure of the RZZ complex and molecular basis of Spindly-driven corona assembly at human kinetochores  
Tobias Raisch, Giuseppe Ciossani, Ennio d’Amico, Verena Cmentowski, Sara Carmignani, Stefano Maffini, Felipe Merino, Sabine Wohlgemuth, Ingrid R. Vetter, Stefan Raunser, Andrea Musacchio   
https://doi.org/10.15252/embj.2021110411
	
A typical workflow to simulate cytoskeletal systems with Cytosim  
Carlos A. Lugo, Eashan Saikia, Francois Nedelec  
https://doi.org/10.48550/arXiv.2205.13852

Cross-linker design determines microtubule network organization by opposing motors. Agosto 2022  
Gil Henkin, Wei-Xiang Chew, François Nédélec Thomas Surrey  
https://doi.org/10.1073/pnas.2206398119


A typical workflow to simulate cytoskeletal systems with Cytosim  
Carlos A. Lugo, Eashan Saikia, Francois Nedelec  
https://doi.org/10.48550/arXiv.2205.13852

Conformational transitions of the Spindly adaptor underlie its interaction with Dynein and Dynactin  
Ennio A. d’Amico, Misbha Ud Din Ahmad, Verena Cmentowski, Mathias Girbig, Franziska Müller, Sabine Wohlgemuth, Andreas Brockmeyer, Stefano Maffini, Petra Janning, Ingrid R. Vetter,  Andrew P. Carter, Anastassis Perrakis, Andrea Musacchio  
https://doi.org/10.1083/jcb.202206131 


# 2021

Reconstitution and use of highly active human CDK1:Cyclin-B:CKS1 complexes  
Pim J. Huis in 't Veld, Sabine Wohlgemuth, Carolin Koerner, Franziska Müller, Petra Janning, Andrea Musacchio  
https://doi.org/10.1002/pro.4233


