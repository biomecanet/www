---
title: "BioMecaNet"
date: 2021-09-03T13:07:14+02:00
draft: false
cascade:
  featured_image: '/img/featured.png'
---

Welcome to BioMecaNet's website!

**Structure of the [Rod](https://www.ebi.ac.uk/interpro/entry/InterPro/IPR019527/)-[Zw10](https://www.ebi.ac.uk/interpro/entry/InterPro/IPR009361/)-[Zwilch](https://www.ebi.ac.uk/interpro/entry/InterPro/IPR018630/) complex solved!**  

{{< video autoplay="true" loop="true" src="rzz_structure_small" >}}  
[Raisch et al.](https://www.biorxiv.org/content/10.1101/2021.12.03.471119v1)  